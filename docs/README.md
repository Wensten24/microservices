# Microservices

## Software Architectures

### What is a Software Architecture?

> Software architecture is simply, the organization of a system. This organization includes all components, how they interact with each other, the environment in which they operate, and the principles used to design the software. In many cases, it can also include the evolution of the evolution of the software into future. 

>Software architecture is designed with a specific mission or missions in mind. That mission has to be accomplished without hindering the missions of other tools or devices. The behavior and structure of the software impact significant decisions, so they need to be appropriately rendered and built for the best possible results. Main used common architectures are:
- Monolithic Architecture
- Service Oriented Architecture
- Microservice Architecture

#### Monolithic Architecture:
> An application built as a single deployable unit. (Also known simply as a monolith.) A monolithic application is often associated with a database and a client-side user interface.

#### Microservices Architecture
**Microservices** are a cloud native architectural in which s single application is composed of many loosely coupled and independently deployable smaller components. or services. Those services typically

- Have their own technology stack, inclusive of the database and data management model;
- communicate with one another over a combination of REST APIs, event streaming and message;
- are organized by business capability, with the line separating services often referred to as a bounded context.

While much of the discussion about **microservices** has revolved around architectural definitions and characteristics, their value can be more commonly understood through fairly simple business and organizational benefits:

- Code can be updated more easily - new features or functionality can be added without touching the entire application
- Teams can use different stacks and different programming languages for different components
- Components can be scaled independently of one another, reducing the waste and cost associated with having to scale entire applications because a single feature might be facing too much load

Microservices might also be understood by what they are not, The two comparison drawn most frequently with **microservices architecture** are **monolithic architecture** and **service-oriented architecture (SOA)**

### Why microservice is better than service oriented architecture

> The difference between microservice and SOA can be a bit less clear, when compared to monolithic architecture. Because its' easier to consider the difference as one of scope.  SOA was an enterprise-wide effort to standardize the way all web services in an organization talk to, and integrate with each other (Enterprise service bus (ESB)), whereas microservices architecture is application specific.

> So, how microservice benefit the organization
1. Independently deployable
2. Right tool for the job
3. Precise Scaling

#### Independently deployable

Because the services are smaller and independently deployable, it no longer requires an act of congress in-order to change a line of code or add a new feature in application.
Microservices gives a promises to organization an antidote to frustrations occurred while having small changes takes huge amount of time, So this approach that better facilitates speed and agility

But speed isn't the only value of designing services this way. A common emerging organizational model it to bring together cross-functional teams around a business problem, service/product. The microservices model fits neatly with this trend because it enable and create small, cross functional team around one service or a collection of services and have them operate in an agile fashion

Mirco services loose coupling also builds a degree of fault isolation and better resilience into applications, which makes it easier for a new member to understand the code base and contribute to it quickly.

#### Right tool for the job

So normally, or in the beginning an application shares a common stack with a large, relational database supporting the entire application, and the approach has so many drawbacks, which like it has share common stack, data model and database even if there is a clear, better tool for the job for certain elements. It makes for bad architecture, and it annoys developers who are constantly aware that a better, more efficient way to build these components is available.

#### Precise Scaling

With microservices, individual services can be individually deployed - but they can individually scaled, as well. The resulting benefit is obvious, microservices require less infrastructure than monolithic applications because they enable precise scaling of only the components that require it, instead of the entire application in the case of monolithic applications

### Microservice both enable and require DevOps

Microservice architecture is often described as optimized for DevOps and continues integration/continues delivery (CI/CD).

A way of looking at the relationship between microservices and DevOps is that microservices architectures actually require DevOps in order to successful. While monolithic applications have a range of drawbacks that has mentioned above, they have the benefit of not being complex distributed system with multiple moving parts and independent tech stacks. In contrast, given the massive increase in complexity, moving parts and dependencies that come with microservices, it would be unwise to approach microservices without significant investments in deployment, monitoring and lifecycle automation.

### API Gateways

Microservices often communicate via API, especially when first establishing state. While its' true that clients and services can communicate with one another directly, API gateways are often a useful intermediary layer, especially as the number of services in an application grows over time. An API gateway acts as a reverse proxy for clients by routing requests, fanning out requests across multiple services, and providing additional security and authentication.

There are multiple technologies that can be used to implement API gateways, including API management platforms, but if the microservices architecture is being implemented using containers and Kubernetes, the gateway is typically implemented using Ingress.

### Microservices and cloud services

Microservices are not necessarily exclusively relevant to cloud computing but there are a few important reasons why they so frequently go together: reasons that go beyond microservices being a popular architectural style for new applications and the cloud being a popular hosting destination for new applications.

Among the primary benefits of microservices architecture are the utilization and cost benefits associated with deploying and scaling components individually. While these benefits would still be present to some extent with on-premises infrastructure, the combination of small, independently scalable components coupled with on-demand, pay-per-use infrastructure is where real cost optimizations can be found.

### Patterns

Within microservices architectures, there are many common and useful design, communication, and integration patterns that help address some of the more common challenges and opportunities, including the following:
	
#### Backend-for-frontend (BFF) pattern: 

>This pattern inserts a layer between the user experience and the resources that experience calls on. For example, an app used on a desktop will have different screen size, display, and performance limits than a mobile device. The BFF pattern allows developers to create and support one backend type per user interface using the best options for that interface, rather than trying to support a generic backend that works with any interface but may negatively impact frontend performance.

#### Entity and aggregate patterns: 

>An entity is an object distinguished by its identity. For example, on an e-commerce site, a Product object might be distinguished by product name, type, and price. An aggregate is a collection of related entities that should be treated as one unit. So, for the e-commerce site, an Order would be a collection (aggregate) of products (entities) ordered by a buyer. These patterns are used to classify data in meaningful ways.

#### Service discovery patterns: 

>These help applications and services find each other. In a microservices architecture, service instances change dynamically due to scaling, upgrades, service failure, and even service termination. These patterns provide discovery mechanisms to cope with this transience. Load balancing may use service discovery patterns by using health checks and service failures as triggers to rebalance traffic.

#### Adapter microservices patterns: 

>Think of adapter patterns in the way you think of plug adapters that you use when you travel to another country. The purpose of adapter patterns is to help translate relationships between classes or objects that are otherwise incompatible. An application that relies on third-party APIs might need to use an adapter pattern to ensure the application and the APIs can communicate.
